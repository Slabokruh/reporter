import React from 'react';
import { Link } from 'react-router-dom';

// import Icon from '../icon';

const logo = (props) => {
    return (
        <div className="logo">
            <Link to="/">
                {/* <Icon type="logo" iconWidth="163" iconHeight="30" /> */}
            </Link>
        </div>
    )
}

export default logo;