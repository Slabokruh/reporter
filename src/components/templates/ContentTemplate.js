import React, { Component } from 'react';
import { Fragment } from 'react'; 
import PropTypes from 'prop-types';
import Footer from '../footer/Footer';
import Header from '../header/Header';
import Filter from '../header/Filter';

class ContentTemplate extends Component {
    static propTypes = {
        children: PropTypes.element.isRequired
    }
    
    render() {
        return (
            <Fragment>
                <header>
                    <Header {...this.props}/>
                   
                </header>
                <Filter><div style={{height: "100px", backgroundColor: "red"}}></div></Filter>
                <main className="main">
                    <div className="container">
                        { this.props.children }
                    </div>
                </main>
                <footer className='footer'>
                    <Footer />
                </footer>
            </Fragment>
        )
    }
}

export default ContentTemplate;