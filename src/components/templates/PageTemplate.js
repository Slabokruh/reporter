import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

import Helmet from 'react-helmet';

export class _PageTemplate extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  render() {
    const {
      children
    } = this.props;

    return (
      <Fragment>
        <Helmet
          title="Home"
        />
        {
          children
        }
      </Fragment>
    );
  }
}

export default withRouter(_PageTemplate);
