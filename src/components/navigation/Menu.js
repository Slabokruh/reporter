import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import Icon from '../icon';


class Menu extends Component {
    state = {
        menuList : [
            {
                path: '/',
                title: 'Summary',
                iconType: 'summary',
                iconWidth: 16,
                iconHeight: 13
            },
            {
                path: '/hours',
                title: 'Hours',
                iconType: 'hours',
                iconWidth: 13,
                iconHeight: 15
            },
            {
                path: '/projects',
                title: 'Projects',
                iconType: 'edit',
                iconWidth: 15,
                iconHeight: 13
            },
            {
                path: '/reports',
                title: 'Time reports',
                iconType: 'timeReport',
                iconWidth: 11,
                iconHeight: 15
            },
            {
                path: '/confirmations',
                title: 'Confirmation',
                iconType: 'confirm',
                iconWidth: 13,
                iconHeight: 10
            },
            {
                path: '/mailer',
                title: 'Mailer',
                iconType: 'mail',
                iconWidth: 13,
                iconHeight: 9
            },
            {
                path: '/compare',
                title: 'Compare',
                iconType: 'compare',
                iconWidth: 15,
                iconHeight: 12
            }
        ]
    }

    render() {

        return (
            <nav className="menu">
                {
                    this.state.menuList.map( item => {
                        return (
                                <NavLink 
                                    className="menu-link"
                                    key={item.title}
                                    activeClassName={this.props.location.pathname !== item.path ? null : "menu-link--active"} 
                                    to={item.path}>
                                    <Icon type={item.iconType} iconWidth={item.iconWidth} iconHeight={item.iconHeight} />
                                    {item.title}
                                </NavLink>
                            )
                    })
                }
            </nav>
        )
    }
}

export default withRouter(Menu);