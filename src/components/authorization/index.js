import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Icon from '../icon/index';

class Authorization extends Component {
    static propTypes = {
        photo: PropTypes.string,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired
    }


    render() {

        const { photo, firstName = 'Michael', lastName = "Jefferson-Carter" } = this.props;

        let photoUser;

        if (photo) {
            photoUser = <img src={photo} alt={`${firstName}  ${lastName}`}/>
        } else {
            photoUser = <Icon type="user" width="22" height="22" />
        }
        
        return (
            <div className="authorization">
                <div className="authorization-photo">
                    { photoUser }
                </div>

                <div className="authorization-name">
                    <span>{firstName}</span>
                    <span>{lastName}</span>
                </div>
            </div>
        )
    }
}

export default Authorization;