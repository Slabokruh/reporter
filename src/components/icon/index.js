import React, { Component, } from 'react';
import PropTypes from 'prop-types';
import ICONS from './icons';

const COLORS = {
    primary: '#2D8EEF',
    secondary: '#B8C1CB',
    secondaryDark: '#758697', 
    light: '#FFF'
}

class Icon extends Component {
    static propTypes = {
        type: PropTypes.string.isRequired,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        color: PropTypes.oneOf([
            'primary',
            'secondary',
            'secondaryDark',
            'light'
        ]),
        className: PropTypes.string
    }
    static defaultProps = {
        type: 'plus',
        width: 10,
        height: 10,
        color: 'light',
        className: ''
    }

    

    render() {
        
        const { type, width, height, color, className } = this.props
        const icon = ICONS[type].format === 'data'
                ? <path d={ICONS[type].path} />
                : ICONS[type].markup;

        return (
            <svg className={className} width={width} height={height} viewBox={ICONS[type].viewbox} aria-labelledby="title" fill={COLORS[color]} stroke={COLORS[color]} >
               {icon}
             </svg>
        );

    }
}

export default Icon;