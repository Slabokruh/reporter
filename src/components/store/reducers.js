import { combineReducers } from 'redux';

import {
  SOME_ASYNC_ACTION,
  SOME_ASYNC_ACTION_SUCCESS,
  SOME_ASYNC_ACTION_FAILURE
} from './actions';

const initiaReporter = {
  isLogin: true,
  isFetching: 0
};

export const reporter = (state = initiaReporter, action) => {
  switch (action.type) {
    case SOME_ASYNC_ACTION:
      return {
        ...state,
        isFetching: state.isFetching + 1
      };

    case SOME_ASYNC_ACTION_SUCCESS:
      return {
        ...state,
        isFetching: state.isFetching - 1
      };

    case SOME_ASYNC_ACTION_FAILURE:
      return {
        ...state,
        isFetching: state.isFetching - 1
      };

    default:
      return state;
  }
};

const rootReducer = combineReducers({
  reporter
});

export default rootReducer;
