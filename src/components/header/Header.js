import React, { Fragment } from 'react';

const Header = (props) => {
    return (
    <div className='container'>
      {
        props.isAuthenticated
        ? 
          <Fragment>
            <span>Here goes a Logo component</span>
            <span>Here goes a Nav component</span>
            <span>Here goes a Auth component</span>
          </Fragment>
        : 
          <span>Logo</span>
      }
      </div>
    )
  }
   
export default Header;