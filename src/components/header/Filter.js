import React, { Component } from "react";
import Button from "../button/index";
import PropTypes from 'prop-types';


class Filter extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      isOpenFilter: false
    };
  }
  openFilterHandle = () => {
    this.setState({ isOpenFilter: !this.state.isOpenFilter });
  };
  render() {
    const { children } = this.props;

    const componentClasses = ["filter-content"];

    if (this.state.isOpenFilter) {
      componentClasses.push("show");
    }

    return (
      <div className="filter">
        <div className="container">
          <div className={componentClasses.join(" ")}>{children}</div>
          <div className="filter-btn">
            {this.state.isOpenFilter ? (
              <Button
                type="simple"
                size="default"
                handleClick={this.openFilterHandle}
                className="btn-filter btn-filter--hide"
              >
                Hide filter
              </Button>
            ) : (
              <Button
                type="simple"
                size="default"
                handleClick={this.openFilterHandle}
                className="btn-filter btn-filter--show"
              >
                Show filter
              </Button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Filter;
