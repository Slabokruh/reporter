import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './store/history';

import PageTemplate from './templates/PageTemplate';
import SummaryPage from './pages/SummaryPage';
import HoursPage from './pages/HoursPage';
import ProjectsPage from './pages/ProjectsPage';
import TimeReportsPage from './pages/TimeReportsPage';
import ConfirmationsPage from './pages/ConfirmationsPage';
import MailerPage from './pages/MailerPage';

export default class App extends Component {
  render() {
    return (
      <Router history={history}>
        <PageTemplate>
          <Switch>
            <Route path="/" exact component={() => <SummaryPage />} />
            <Route path="/hours" exact component={() => <HoursPage />} />
            <Route path="/projects" exact component={() => <ProjectsPage />} />
            <Route path="/reports" exact component={() => <TimeReportsPage />} />
            <Route path="/confirmations" exact component={() => <ConfirmationsPage />} />
            <Route path="/mailer" exact component={() => <MailerPage />} />
          </Switch>
        </PageTemplate>
      </Router>
    );
  }
}
