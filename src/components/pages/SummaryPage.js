import React, { Component } from 'react';
import ContentTemplate from '../templates/ContentTemplate';

class SummaryPage extends Component {
  handleClick = () => {
    console.log('click')
  }
  render() {
    return (
      <div className='page page-summary'>
        <ContentTemplate 
          isAuthenticated={true} // hardcode for now
        >
          <h1>Hello! I am reporter=)</h1>
        </ContentTemplate>
      </div>
    );
  }
}

export default SummaryPage;
