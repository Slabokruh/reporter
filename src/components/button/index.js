import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Icon from './../icon/index';

const TYPES = {
    primary: 'btn-primary',
    border: 'btn-bordered',
    icon: 'btn-icon',
    simple: 'btn-simple'
}

const SIZES = {
    lg: 'btn-large',
    md: 'btn-middle',
    sm: 'btn-small',
    default: 'btn-default'
}

class Button extends Component { 

    static propTypes = {
        type: PropTypes.oneOf([
                'primary',
                'border',
                'icon',
                'simple'
        ]).isRequired,
        size: PropTypes.oneOf([
            'lg',
            'md',
            'sm',
            'default'
        ]).isRequired,
        className: PropTypes.string,
        withIcon: PropTypes.bool,
        disabled: PropTypes.bool,
        active: PropTypes.bool,
        handleClick: PropTypes.func.isRequired
    }

    static defaultProps = {
        type : 'primary', 
        size : 'default', 
        className : '', 
        disabled : false, 
        active : false,
    }
    
    render() {
        const { 
                type, 
                size, 
                className, 
                disabled, 
                handleClick,
                children,
                active,
                iconType,
                iconWidth,
                iconHeight,
                iconColor,
                withIcon
            } = this.props;
      

    return (
            <button 
                className={`btn ${TYPES[type]} ${SIZES[size]} ${active ? 'active' : '' } ${withIcon ? 'btn--with-icon' : ''} ${className} `}
                disabled={disabled}
                onClick={handleClick}
            >
                {
                    type === 'icon'
                ?
                    <Fragment>
                        {withIcon && children}
                        ‌ <Icon type={iconType} width={iconWidth} height={iconHeight} color={iconColor} />
                    ‌ </Fragment>
                :
                    children
                }
            </button>
        )
    }
}

export default Button;