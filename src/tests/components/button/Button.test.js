import React from 'react';
import { shallow } from 'enzyme';
import Button from './../../components/button/index';
import Icon from './../../components/icon/index';


describe('Button', () => {
    it('should render with correct type', () => {
      const wrapper = shallow(<Button type="primary">Apply</Button>);
      expect(wrapper.find('.btn-primary')).toHaveLength(1);
    });

    it('should render without type with default primary', () => {
      const wrapper = shallow(<Button>Apply</Button>);
      expect(wrapper.find('button').hasClass('btn-primary')).toEqual(true);
    });

    it('should render with props disabled', () => {
      const wrapper = shallow(<Button disabled={true}>Apply</Button>);
      expect(wrapper.props().disabled).toEqual(true);
    });

    it('should render with class - active', () => {
      const wrapper = shallow(<Button active={true}></Button>);
      expect(wrapper.find('button').hasClass('active')).toEqual(true);
    });

    it('should be size without soasing', () => {
      const wrapper = shallow(<Button>Apply</Button>);
      expect(wrapper.find('.btn-default')).toHaveLength(1);
    });

    it('should be with title', () => {
      const wrapper = shallow(<Button type="primary">Apply</Button>);
      expect(wrapper.find('button').text()).toEqual('Apply');
    });
    it('should contain component Icon', () => {
      const wrapper = shallow(<Button type="icon"></Button>);
      expect(wrapper.find(Icon)).toHaveLength(1);
    });
});


