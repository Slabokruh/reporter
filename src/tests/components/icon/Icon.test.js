import React from 'react';
import { shallow } from 'enzyme';
import Icon from './../../components/icon/index';

describe('Icon', () => {
    it('should render with default width = 10', () => {
      const wrapper = shallow(<Icon></Icon>);
      expect(wrapper.find('svg').props().width).toEqual(10);
    });
  
    it('should be with default type plus', () => {
      const wrapper = shallow(<Button type="icon"></Button>);
      expect(wrapper.find(Icon).props().type).toEqual('plus');
    });
  
    it('should be with type calendar', () => {
      const wrapper = shallow(<Button type="icon" iconType="calendar"></Button>);
      expect(wrapper.find(Icon).props().type).toEqual('calendar');
    });
  
    it('should be with color default white', () => {
      const wrapper = shallow(<Button type="icon" iconType="calendar" ></Button>);
      expect(wrapper.find(Icon).props().color).toEqual('light');
    });
})