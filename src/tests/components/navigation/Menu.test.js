import React from 'react';
import { shallow } from 'enzyme';
import Menu from '../../../components/navigation/Menu';

describe('Menu', () => {
    it('shoud render with active class after click', () => {
        const wrapper = shallow(<Menu />);
        wrapper.find('menu-link').forEach( (node) => {
            node.simulate('click');
            expect(node).hasClass('menu-link--active').toEqual(true);
        })
    })
} )