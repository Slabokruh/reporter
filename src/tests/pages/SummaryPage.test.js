import React from 'react';
import { shallow } from 'enzyme';
import SummaryPage from '../../components/pages/SummaryPage';

describe('SummaryPage', () => {
  it('should render', () => {
    const wrapper = shallow(<SummaryPage />);
    expect(wrapper.find('.page-summary')).toHaveLength(1);
  });
});
