import React from 'react';
import { shallow } from 'enzyme';
import HoursPage from '../../components/pages/HoursPage';

describe('HoursPage', () => {
  it('should render', () => {
    const wrapper = shallow(<HoursPage />);
    expect(wrapper.find('.page-hours')).toHaveLength(1);
  });
});
