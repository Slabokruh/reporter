import React from 'react';
import { shallow } from 'enzyme';
import MailerPage from '../../components/pages/MailerPage';

describe('MailerPage', () => {
  it('should render', () => {
    const wrapper = shallow(<MailerPage />);
    expect(wrapper.find('.page-mailer')).toHaveLength(1);
  });
});
