import React from 'react';
import { shallow } from 'enzyme';
import ProjectsPage from '../../components/pages/ProjectsPage';

describe('ProjectsPage', () => {
  it('should render', () => {
    const wrapper = shallow(<ProjectsPage />);
    expect(wrapper.find('.page-projects')).toHaveLength(1);
  });
});
