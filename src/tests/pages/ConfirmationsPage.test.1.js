import React from 'react';
import { shallow } from 'enzyme';
import ConfirmationsPage from '../../components/pages/ConfirmationsPage';

describe('ConfirmationsPage', () => {
  it('should render', () => {
    const wrapper = shallow(<ConfirmationsPage />);
    expect(wrapper.find('.page-confirmations')).toHaveLength(1);
  });
});
