import React from 'react';
import { shallow } from 'enzyme';
import TimeReportsPage from '../../components/pages/TimeReportsPage';

describe('TimeReportsPage', () => {
  it('should render', () => {
    const wrapper = shallow(<TimeReportsPage />);
    expect(wrapper.find('.page-reports')).toHaveLength(1);
  });
});
