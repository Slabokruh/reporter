/* global require */

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const cssbeautify = require('gulp-cssbeautify');


gulp.task('sass', () => {
  gulp.src('./src/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 20 versions']
    }))
    .pipe(sourcemaps.write())
    .pipe(cssbeautify())
    .pipe(gulp.dest('../public'));
});

gulp.task('build', () => {
  gulp.src('./src/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 20 versions']
    }))
    .pipe(sourcemaps.write())
    .pipe(cssbeautify())
    .pipe(gulp.dest('../build'));
});

gulp.task('watch', () => {
  gulp.watch(['./src/*.scss'], ['sass']);
});

// gulp.task('default', ['sass']);
